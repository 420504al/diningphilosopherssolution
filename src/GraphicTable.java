import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

@SuppressWarnings("serial")
public class GraphicTable extends Frame implements WindowListener, ActionCallBackEvents {

    private Point center;
    private GraphicPlate plates[];
    private GraphicChopstick chops[];
    private PhilosopherHelper philosopherHelper;


    public GraphicTable() {
        super();

        addWindowListener(this);
        setTitle("Dining Philosophers");
        setSize(200, 200);
        setBackground(Color.darkGray);

        center = new Point(getSize().width / 2, getSize().height / 2);

        plates = new GraphicPlate[PhilosopherHelper.MAX];
        for (int i = 0; i < PhilosopherHelper.MAX; i++) {
            plates[i] = new GraphicPlate(i, center, new Point(center.x, center.y - 70), 20);
        }

        chops = new GraphicChopstick[PhilosopherHelper.MAX];
        for (int i = 0; i < 5; i++) {
            chops[i] = new GraphicChopstick(i, center,
                    new Point(center.x, center.y - 70),
                    new Point(center.x, center.y - 40));
        }

        philosopherHelper = new PhilosopherHelper();
        philosopherHelper.addActionCallBackListener(new ActionCallBackListener(this));
        philosopherHelper.start();

        setVisible(true);
        setResizable(false);

    }

    public void windowOpened(WindowEvent evt) {
    }

    public void windowClosing(WindowEvent evt) {
        System.exit(0);
    }

    public void windowClosed(WindowEvent evt) {
    }

    public void windowIconified(WindowEvent evt) {
    }

    public void windowDeiconified(WindowEvent evt) {
    }

    public void windowActivated(WindowEvent evt) {
    }

    public void windowDeactivated(WindowEvent evt) {
    }

    public void takeChopstick(int phID, int chID) {
        System.out.println(String.format("repainting ph %d - color %d ", phID, chID ));
        chops[chID].setColor(phID);
        repaint();
    }

    public void releaseChopstick(int philosopherID, int chID) {
        chops[chID].setColor(-1);
        repaint();
    }

    public void repaintPlates(int philosopherID, int colorID) {
        plates[philosopherID].setColor(colorID);
        repaint();
    }

    public void paint(Graphics g) {
        for (int i = 0; i < 5; i++) {
            plates[i].draw(g);
            chops[i].draw(g);
        }
    }
}
