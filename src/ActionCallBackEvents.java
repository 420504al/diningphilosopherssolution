/**
 * Created by lacouf on 2017-09-20.
 */
public interface ActionCallBackEvents {

    void repaintPlates(int philosopherID, int colorID);
    void takeChopstick(int philosopherID, int colorID);
    void releaseChopstick(int philosopherID, int colorID);
}
