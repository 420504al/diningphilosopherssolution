import java.util.ArrayList;
import java.util.List;

/**
 * Created by lacouf on 2017-09-20.
 */
public class PhilosopherHelper implements PhilosopherActions {

    public static final int MAX = 5;
    private static int compte = 0;
    private static int enAttente = 0;
    
    private boolean chopsticks[];

    private Philosopher p0;
    private Philosopher p1;
    private Philosopher p2;
    private Philosopher p3;
    private Philosopher p4;

    private List<ActionCallBackListener> actionCallBackList = new ArrayList<>();

    public PhilosopherHelper() {
        chopsticks = new boolean[MAX];
        for (int i = 0; i <= MAX-1; i++)
            chopsticks[i] = true;
    }

    public void start() {
        p0 = new Philosopher(0, this, 0, 4);
        p1 = new Philosopher(1, this, 1, 0);
        p2 = new Philosopher(2, this, 2, 1);
        p3 = new Philosopher(3, this, 3, 2);
        p4 = new Philosopher(4, this, 4, 3);

        int timeToEatMaxMs = 20;
        int timeToThinkMaxMx = 20;
        int timeNextForkMs = 20;

        resetPhilosopherTimings(p0, timeToEatMaxMs, timeToThinkMaxMx, timeNextForkMs);
        resetPhilosopherTimings(p1, timeToEatMaxMs, timeToThinkMaxMx, timeNextForkMs);
        resetPhilosopherTimings(p2, timeToEatMaxMs, timeToThinkMaxMx, timeNextForkMs);
        resetPhilosopherTimings(p3, timeToEatMaxMs, timeToThinkMaxMx, timeNextForkMs);
        resetPhilosopherTimings(p4, timeToEatMaxMs, timeToThinkMaxMx, timeNextForkMs);

        p0.start();
        p1.start();
        p2.start();
        p3.start();
        p4.start();
    }

    private void resetPhilosopherTimings(Philosopher philosopher, int
            timeToEatMaxMs, int timeToThinkMaxMs, int
            timeNextForkMs ) {
        philosopher.setTimeEat_max(timeToEatMaxMs);
        philosopher.setTimeThink_max(timeToThinkMaxMs);
        philosopher.setTimeNextFork(timeNextForkMs);
    }

    private void waitFinish() {
        while (p0.isAlive() || p1.isAlive() || p2.isAlive() || p3.isAlive()
                || p4.isAlive()) {
            ;
        }
    }

    public boolean isFinished() {
        return !p0.isAlive() && !p1.isAlive() && !p2.isAlive() && !p3.isAlive
                () && !p4.isAlive();
    }

    public void addActionCallBackListener(ActionCallBackListener actionCallBackListener) {
        actionCallBackList.add(actionCallBackListener);
    }

    private void repaintPlatesNotify(int philosopherID, int colorID) {
        for (ActionCallBackListener listener : actionCallBackList) {
            listener.doRepaintPlates(philosopherID, colorID);
        }
    }

    private void takeChopstickNotify(int philosopherID,
                                     int colorID) {
        for (ActionCallBackListener listener : actionCallBackList) {
            listener.takeChopstick(philosopherID, colorID);
        }
    }

    private void releaseChopstickNotify(int philosopherID,
                                        int colorID) {
        for (ActionCallBackListener listener : actionCallBackList) {
            listener.releaseChopstick(philosopherID, colorID);
        }
    }

    public synchronized void becomesHungry(int phID) {
        while (compte == MAX || enAttente > 0) {
            attente();
        }
        compte++;
        repaintPlatesNotify(phID, phID);
    }

    private void attente() {
        try {
            enAttente++;
            wait();
            enAttente--;
        } catch (InterruptedException e) {
            System.out.println(e);
        }
    }

    public synchronized void doThinking(int phID) {
        repaintPlatesNotify(phID, -1);
        compte--;
        notify();
    }

    public synchronized void take(int c) {
        while (!chopsticks[c]) {
            try {
                wait();
            } catch (InterruptedException e) {
                System.out.println("boom !");
            }
        }
        chopsticks[c] = false;

    }

    public synchronized void release(int c) {
        chopsticks[c] = true;
        notifyAll();

    }

    public void takeChopstick(int phID, int chID) {
        takeChopstickNotify(phID, chID);
    }

    public void releaseChopstick(int phID, int chID) {
        releaseChopstickNotify(phID, chID);
    }
}
