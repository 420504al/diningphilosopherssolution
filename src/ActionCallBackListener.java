/**
 * Created by lacouf on 2017-09-20.
 */
public class ActionCallBackListener {

    private ActionCallBackEvents actionCallBackEvents;

    public ActionCallBackListener(ActionCallBackEvents actionCallBackEvents) {
        this.actionCallBackEvents = actionCallBackEvents;
    }

    public void doRepaintPlates(int philosopherID, int colorID) {
        actionCallBackEvents.repaintPlates(philosopherID, colorID);
    }

    public void takeChopstick(int philosopherID, int colorID) {
        actionCallBackEvents.takeChopstick(philosopherID, colorID);
    }

    public void releaseChopstick(int philosopherID, int colorID) {
        actionCallBackEvents.releaseChopstick(philosopherID, colorID);
    }
}
