public class Philosopher extends Thread {
    final static int LAST_PHILOSOPHER = 4;
    private int timeThink_max = 1000;
    private int timeNextFork = 1000;
    private int timeEat_max = 3000;
    private int iterationMax = 100;
    private PhilosopherActions philosopherActions;
    private int left;
    private int right;
    private int id;

    Philosopher(int id, PhilosopherActions philosopherActions, int left, int right) {
        this.id = id;
        this.philosopherActions = philosopherActions;
        this.left = left;
        this.right = right;
        setName("Philosopher " + id);
    }

    public void setTimeThink_max(int timeThink_max) {
        this.timeThink_max = timeThink_max;
    }

    public void setTimeNextFork(int timeNextFork) {
        this.timeNextFork = timeNextFork;
    }

    public void setTimeEat_max(int timeEat_max) {
        this.timeEat_max = timeEat_max;
    }

    public void run() {
        for (int i = 0; i < iterationMax; i++) {
            philosopherThinks(i);
            philosopherBecomeHungry();
            philosopheTakesChopsticks();
            philosopherEats();
            philosopherReleaseChopsticks();
        }
    }

    private void philosopherThinks(int i) {
        philosopherActions.doThinking(id);
        System.out.println(getName() + " thinks (iteration) " + i);
        doSleep((long) (Math.random() * timeThink_max));
        System.out.println(getName() + " finished thinking (iteration) " + i);
    }

    private void philosopherBecomeHungry() {
        System.out.println(getName() + " is hungry");
        philosopherActions.becomesHungry(id);
    }

    private void philosopheTakesChopsticks() {
        if (id != LAST_PHILOSOPHER)
            askForForkNormal();
        else
            askForForkLastPhilosopher();
    }

    private void philosopheTakesChopsticksDeadLock() {
        askForForkNormal();
    }

    private void askForForkNormal() {
        wantsFork(getName() + " wants left chopstick", left, getName() + " got left chopstick");
        doSleep(timeNextFork);
        wantsFork(getName() + " wants right chopstick", right, getName() + " got right chopstick");
    }

    private void askForForkLastPhilosopher() {
        wantsFork(getName() + " wants right chopstick", right, getName() + " got right chopstick");
        doSleep(timeNextFork);
        wantsFork(getName() + " wants left chopstick", left, getName() + " got left chopstick");
    }

    private void wantsFork(String x, int side, String x2) {
        System.out.println(x);
        philosopherActions.take(side);
        philosopherActions.takeChopstick(id, side);
        System.out.println(x2);
    }

    private void philosopherEats() {
        final double timeToSleep = Math.random() * timeEat_max;
        System.out.println(getName() + " eats for " + timeToSleep);
        doSleep((long) timeToSleep);

        System.out.println(getName() + " finished eating");
    }

    private void philosopherReleaseChopsticks() {
        philosopherActions.releaseChopstick(id, left);
        philosopherActions.release(left);
        System.out.println(getName() + " released left chopstick");

        philosopherActions.releaseChopstick(id, right);
        philosopherActions.release(right);
        System.out.println(getName() + " released right chopstick");
    }

    private void doSleep(long millis) {
        try {
            sleep(millis);

        } catch (InterruptedException e) {
            System.out.println(e);
        }
    }

}
