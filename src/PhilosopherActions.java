
public interface PhilosopherActions {

    void doThinking(int id);
    void becomesHungry(int id);
    void take(int side);
    void takeChopstick(int id, int side);
    void release(int left);
    void releaseChopstick(int id, int left);

}
