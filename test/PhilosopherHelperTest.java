import org.junit.Test;

/**
 * Created by lacouf on 2017-09-20.
 */
public class PhilosopherHelperTest {

    @Test(timeout=9000)
    public void testDeadLock() {
        PhilosopherHelper helper = new PhilosopherHelper();
        helper.addActionCallBackListener(new ActionCallBackListener(new
                ActionCallBackEvents() {

                    @Override
                    public void repaintPlates(int philosopherID, int colorID) {
                        System.out.println(String.format("repaint plates " +
                                "phID: %d - " +
                                "colorID: %d", philosopherID, colorID));
                    }

                    @Override
                    public void takeChopstick(int philosopherID, int colorID) {
                        System.out.println("take chopstick");
                    }

                    @Override
                    public void releaseChopstick(int philosopherID, int colorID) {
                        System.out.println("release chopstick");
                    }
                }));
        helper.start();
        while(!helper.isFinished());
    }

}
